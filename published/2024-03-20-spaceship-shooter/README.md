# Корабль-гранатомёт

Космический корабль следует за мышей. Клик мышей - метка для гранаты.

Результат объединения заметок:

1. [2](../../2) для обработки кликов мыши;
2. [6](../../6) для загрузки картинок;
3. [7](../../7) для корректной обработки времени в игровом цикле;
4. [8](../../8) для постепенного разворота корабля и снарядов.



[https://youtu.be/FctZKxtDqYo?si=6iMXAWQShrukm4SR](https://youtu.be/FctZKxtDqYo?si=6iMXAWQShrukm4SR)
