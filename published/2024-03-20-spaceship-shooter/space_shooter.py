import pygame as pg

from math import copysign
from pygame.math import Vector2, disable_swizzling
from pygame.time import Clock

from settings import (
    UPDATE_RATE, MIN_UPDATE_RATE, SHIP_SPEED, SHIP_ANGLE_SPEED,
    BULLET_SPEED, BULLET_ANGLE_SPEED
)


class Bullet:
    origin = pg.image.load('bullet.png')

    def __init__(self, surface, start_position, destination, direction):
        self.surface = surface
        self.position = Vector2(start_position)
        self.destination = Vector2(destination)
        self.speed = direction * BULLET_SPEED

        self.is_alive = True
        self.is_landed = False
        self.time_landed = 1

        self.angle_speed = BULLET_ANGLE_SPEED

        self.image = self.origin

    def update(self, delta_time):
        if not self.is_alive:
            return

        if self.is_landed:
            self.time_landed -= delta_time
            if self.time_landed <= 0:
                self.is_alive = False
            return

        direction_to_dest = (self.destination - self.position).normalize()

        if (self.position - self.destination).length() <= (self.speed * delta_time).length():
            self.speed = direction_to_dest.normalize() * BULLET_SPEED
        else:
            angle = self.speed.angle_to(direction_to_dest)
            if abs(angle) > 180:
                angle = -copysign((360 - abs(angle)), angle)
            self.speed = self.speed.rotate(
                copysign(min(abs(angle), self.angle_speed * delta_time), angle))

        inc = self.speed * delta_time
        if inc.length() < (self.position - self.destination).length():
            self.position += inc
        else:
            self.position = self.destination
            self.is_landed = True

    def render(self):
        pg.draw.circle(self.surface, (222, 22, 22), self.destination, 10, 1)
        self.surface.blit(self.image, self.image.get_rect(center=self.position))


class SpaceShip:
    def __init__(self, surface, position):
        self.surface = surface

        self.position = Vector2(position)
        self.speed = SHIP_SPEED
        self.direction = Vector2(0, -1)
        
        self.angle_speed = SHIP_ANGLE_SPEED

        self.origin = pg.image.load('ship.png')
        self.image = self.origin

    def update(self, delta_time, to_point):
        direction_to_point = (to_point - self.position).normalize()
        angle = self.direction.angle_to(direction_to_point)
        if abs(angle) > 180:
            angle = -copysign((360 - abs(angle)), angle)
        self.direction = self.direction.rotate(
            copysign(min(abs(angle), self.angle_speed * delta_time), angle))

        self.image = pg.transform.rotate(self.origin, self.direction.angle_to(Vector2(0, -1)))

        if self.position.distance_to(to_point) > 10:
            self.position += (self.direction * self.speed) * delta_time

    def render(self):
        radius = 25
        #pg.draw.circle(self.surface, (230, 230, 230), self.position, radius, width=1)
        #pg.draw.line(self.surface, (250, 250, 0), self.position, self.position + self.direction * (radius + 10))

        self.surface.blit(self.image, self.image.get_rect(center=self.position))

    def fire(self, to_point):
        return Bullet(
            self.surface,
            self.position + self.direction * 15,
            to_point,
            self.direction
        )


class Game:
    def __init__(self, surface):
        self.surface = surface

        self.bg_color = (5, 34, 39)

        center = (self.surface.get_width() / 2, self.surface.get_height() / 2)
        self.ship = SpaceShip(surface, center)

        self.bullets = []

        self.time_accumulator = 0
        self.clock = Clock()

        self.destination_point = center

        self.pressed = False

        self.working = True

    def handle_events(self):
        for e in pg.event.get():
            if e.type == pg.QUIT:
                self.working = False
            elif e.type == pg.KEYDOWN and e.key == pg.K_ESCAPE:
                self.working = False

        self.destination_point = Vector2(pg.mouse.get_pos())

        if self.pressed and not pg.mouse.get_pressed()[0]:
            self.pressed = False
            self.bullets.append(self.ship.fire(self.destination_point))
        elif not self.pressed and pg.mouse.get_pressed()[0]:
            self.pressed = True

    def update(self, delta_time):
        self.time_accumulator += delta_time

        while self.time_accumulator >= UPDATE_RATE:
            self.time_accumulator -= UPDATE_RATE

            self.ship.update(UPDATE_RATE, self.destination_point)

            for bullet in self.bullets:
                bullet.update(UPDATE_RATE)
            self.bullets = [b for b in self.bullets if b.is_alive]

    def render(self):
        self.surface.fill(self.bg_color)

        for bullet in self.bullets:
            bullet.render()
        self.ship.render()

        pg.display.update()

    def tick(self):
        return self.clock.tick() / 1000

    def run(self):
        self.tick()
        delta_time = 0
        while self.working:
            self.handle_events()
            self.update(min(MIN_UPDATE_RATE, delta_time))
            self.render()
            delta_time = self.tick()

def main():
    pg.init()

    size = (1920, 1080)
    flags = pg.FULLSCREEN

    Game(pg.display.set_mode(size, flags=flags)).run()

    pg.quit()


if __name__ == "__main__":
    main()
