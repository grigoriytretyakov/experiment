import pygame

from pygame.math import Vector2


def main():
    pygame.init()

    surface = pygame.display.set_mode((800, 450))

    font = pygame.font.Font('Boxy-Bold.ttf', 24)
    rendered = font.render('Ok?', False, (50, 50, 50))

    working = True
    while working:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                working = False
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_ESCAPE:
                    working = False
                else:
                    if e.unicode.strip():
                        rendered = font.render(e.unicode.strip(), False, (50, 50, 50))

        surface.fill((200, 200, 200))
        surface.blit(rendered, Vector2(pygame.mouse.get_pos()) + Vector2(20, 20))

        pygame.display.update()

    pygame.quit()


if __name__ == "__main__":
    main()
