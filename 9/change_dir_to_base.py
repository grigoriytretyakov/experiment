'''
Смена текущей директории на директорию, в которой лежит скрипт.
Это упрощает работу со снипеттами, как с ящиком заметок.
'''


import os
import pygame as pg

from pygame.surface import Surface


class Game:
    def __init__(self, surface: Surface) -> None:
        self.surface = surface
        self.center = self.surface.get_width() / 2, self.surface.get_height() / 2
        self.image = pg.image.load('land.png')

    def render(self):
        self.surface.fill((0, 0, 0))
        self.surface.blit(self.image, self.image.get_rect(center=self.center))

        pg.display.update()

    def run(self):
        working = True
        while working:
            for e in pg.event.get():
                if e.type == pg.QUIT or (e.type == pg.KEYDOWN and e.key == pg.K_ESCAPE):
                    working = False
            self.render()

def chdir():
    path = os.path.dirname(os.path.realpath(__file__))
    print('Change dir to: <{}>'.format(path))
    os.chdir(path)


def main():
    pg.init()
    Game(pg.display.set_mode((800, 450))).run()
    pg.quit()


if __name__ == "__main__":
    chdir()
    main()
