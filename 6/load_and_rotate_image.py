'''
Изображение вращается вокруг своего центра. Если нажать любую клавишу, то направление
вращения меняется на противоположное.
'''


import pygame as pg 

from math import copysign
from pygame.time import Clock


class Game:
    def __init__(self, surface):
        self.surface = surface

        self.background = (121, 103, 85)

        self.origin = pg.image.load('tank.png')
        self.center = (
            (self.surface.get_width() - self.origin.get_width()) / 2,
            (self.surface.get_height() - self.origin.get_height()) / 2
        )
        self.angle = 0
        self.angle_speed = 3
        self.tank = pg.transform.rotate(self.origin, self.angle)

        self.clock = Clock()
        self.fps = 60

        self.working = True

    def handle_events(self):
        for e in pg.event.get():
            if e.type == pg.QUIT:
                self.working = False
            elif e.type == pg.KEYDOWN:
                if e.key == pg.K_ESCAPE:
                    self.working = False
                else:
                    # change direction
                    self.angle_speed = -self.angle_speed

    def update(self):
        self.angle += self.angle_speed
        self.angle = copysign(abs(self.angle) % 360, self.angle)
        self.tank = pg.transform.rotate(self.origin, self.angle)

    def render(self):
        self.surface.fill(self.background)
        '''
        Смысл в том, что при вращении прямоугольного изображения меняются размеры Rect вокруг
        него. Из-за этого изображение может смещаться во время вращения, если его рисовать на экране
        кодом self.surface.blit(self.tank, self.center). Но если взять Rect с центром в указанной точке,
        то изображение будет вращаться как надо
        '''
        self.surface.blit(self.tank, self.tank.get_rect(center=self.center))
        pg.display.update()

       
    def run(self):
        while self.working:
            self.handle_events()
            self.update()
            self.render()

            self.clock.tick(self.fps)

def main():

    pg.init()

    size = (800, 450)
    display_surface = pg.display.set_mode(size)
    Game(display_surface).run()

    pg.quit()


if __name__ == "__main__":
    main()
